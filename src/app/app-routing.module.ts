import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './modules/admin/admin.component';
import { PontoComponent } from './modules/ponto/ponto.component';
import { AuthGuard } from './utility/app.guard';

const routes: Routes = [
  { path: '', component: PontoComponent, canActivate: [AuthGuard]},
  { 
    path: 'admin', 
    component: AdminComponent, 
    canActivate: [AuthGuard],
    data: { roles: ['admin']}
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
