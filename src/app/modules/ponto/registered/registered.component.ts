import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { KeycloakService } from 'keycloak-angular';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
import { WorkdayDataService } from 'src/app/service/workday/workday-data.service';

const moment = _rollupMoment || _moment;

export const FORMATACAO_DATA = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-registered',
  templateUrl: './registered.component.html',
  styleUrls: ['./registered.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: FORMATACAO_DATA },
  ],
})
export class RegisteredComponent implements OnInit {
  @Input() data: any;
  @Input() expedienteMensal: any;

  mesAtual: number = 0;
  anoAtual: number = 0;

  constructor(
    private dado: WorkdayDataService,
    private keycloakService: KeycloakService
  ) {}

  ngOnInit(): void {
    this.data = new FormControl(moment());
    this.mesAtual = this.data.value.month();
    this.anoAtual = this.data.value.year();
    this.encontrarExpediente(this.mesAtual, this.anoAtual);
  }

  public tratarAnoEscolhido(ano: Moment) {
    const valorData = this.data.value;
    valorData.year(ano.year());
    this.data.setValue(valorData);
  }

  public tratarMesEscolhido(mes: Moment, datepicker: MatDatepicker<Moment>) {
    const valorData = this.data.value;
    valorData.month(mes.month());
    this.data.setValue(valorData);
    this.encontrarExpediente(valorData.month(), valorData.year());
    datepicker.close();
  }

  private async encontrarExpediente(mes: number, ano: number) {
    try {
      await this.dado
        .getExpedienteDoMes(this.keycloakService.getUsername(), mes + 1, ano)
        .toPromise()
        .then((dados) => {
          this.expedienteMensal = JSON.parse(dados);
        });
    } catch (error) {
      console.log('Erro ao obter expediente do colaborador informado');
    }
  }

  public verificarSeHoje = (data: string) => {
    const dia = new Date(data);

    const hoje = new Date();

    return (
      dia.getDate() == hoje.getDate() &&
      dia.getMonth() == hoje.getMonth() &&
      dia.getFullYear() == hoje.getFullYear()
    );
  };

  public formatarData(data: string) {
    var dateData = new Date(data);

    let dia = '' + dateData.getDate();
    let mes = '' + dateData.getMonth() + 1;
    const ano = dateData.getFullYear();

    if (mes.length < 2) {
      mes = '0' + mes;
    }
    if (dia.length < 2) {
      dia = '0' + dia;
    }

    return [dia, mes, ano].join('/');
  }
}
