import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PontoComponent } from './ponto.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import { RegisterModule } from '../ponto/register/register.module';
import { RegisteredModule } from './registered/registered.module';
import { NavbarModule } from '../navbar/navbar.module';

@NgModule({
  declarations: [PontoComponent],
  imports: [
    CommonModule,
    NavbarModule,
    MatSidenavModule,
    RegisterModule,
    RegisteredModule
  ],
  exports: [
    PontoComponent,
  ]
})
export class PontoModule { }
