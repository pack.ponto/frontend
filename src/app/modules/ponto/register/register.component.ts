import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { WorkdayDataService } from 'src/app/service/workday/workday-data.service';
import { FormControl } from '@angular/forms';
import moment from 'moment';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  dias: string[] = [
    'Domingo',
    'Segunda-feira',
    'Terça-feira',
    'Quarta-feira',
    'Quinta-feira',
    'Sexta-feira',
    'Sábado',
  ];
  meses: string[] = [
    'jan.',
    'fev.',
    'mar.',
    'abr.',
    'maio',
    'jun.',
    'jul.',
    'ago.',
    'set.',
    'out.',
    'nov.',
    'dez.',
  ];

  hoje: Date = new Date();
  dia = this.dias[this.hoje.getDay()];
  mes = this.meses[this.hoje.getMonth()];

  data: string = `${this.dia}, ${this.hoje.getDate()} ${
    this.mes
  } ${this.hoje.getFullYear()}`;

  pontos: any;

  observacoes = new FormControl();

  expedienteMesAtual: any;

  @Output() expedienteAtualizado: EventEmitter<any> = new EventEmitter();
  @Output() dataSelecionada: EventEmitter<any> = new EventEmitter();

  constructor(
    private dado: WorkdayDataService,
    private keycloakService: KeycloakService
  ) {}

  async ngOnInit(): Promise<void> {
    try {
      await this.dado
        .getExpedienteDoDia()
        .toPromise()
        .then((res) => {
          const expediente: any = JSON.parse(res);
          this.pontos = expediente.pontos;
        });
    } catch (error) {
      console.log('Expediente não existente');
    }
  }

  public async registrarHoraDeEntrada(observacoes: string) {
    try {
      await this.dado
        .postHorarioDeEntrada(observacoes)
        .toPromise()
        .then(() => {
          this.dado.getExpedienteDoDia().subscribe((res) => {
            const expediente: any = JSON.parse(res);
            this.pontos = expediente.pontos;
          });
        });
      this.atualizarDrawer();
      this.resetarCampo();
    } catch (error) {
      console.log('Horário de entrada já registrado');
    }
  }

  public async registrarHoraDeAlmoco(observacoes: string) {
    try {
      await this.dado
        .patchHorarioDeAlmoco(observacoes)
        .toPromise()
        .then(() => {
          this.dado.getExpedienteDoDia().subscribe((res) => {
            const expediente: any = JSON.parse(res);
            this.pontos = expediente.pontos;
          });
        });
      this.atualizarDrawer();
      this.resetarCampo();
    } catch (error) {
      console.log('Horário de almoço já registrado');
    }
  }

  public async registrarHoraDeRetorno(observacoes: string) {
    try {
      await this.dado
        .patchHorarioDeRetorno(observacoes)
        .toPromise()
        .then(() => {
          this.dado.getExpedienteDoDia().subscribe((res) => {
            const expediente: any = JSON.parse(res);
            this.pontos = expediente.pontos;
          });
        });
      this.atualizarDrawer();
      this.resetarCampo();
    } catch (error) {
      console.log('Horário de retorno já registrado');
    }
  }

  public async registrarHoraDeSaida(observacoes: string) {
    try {
      await this.dado
        .patchHorarioDeSaida(observacoes)
        .toPromise()
        .then(() => {
          this.dado.getExpedienteDoDia().subscribe((res) => {
            const expediente: any = JSON.parse(res);
            this.pontos = expediente.pontos;
            this.resetarCampo();
          });
        });
      this.atualizarDrawer();
      this.resetarCampo();
    } catch (error) {
      console.log('Horário de saída já registrado');
    }
  }

  private resetarCampo() {
    this.observacoes.reset('');
  }

  private async atualizarDrawer() {
    await this.dado
      .getExpedienteDoMes(
        this.keycloakService.getUsername(),
        this.hoje.getMonth() + 1,
        this.hoje.getFullYear()
      )
      .toPromise()
      .then((dados) => {
        this.expedienteMesAtual = JSON.parse(dados);
      });
    this.expedienteAtualizado.emit(this.expedienteMesAtual);
    this.dataSelecionada.emit(new FormControl(moment()));
  }
}
