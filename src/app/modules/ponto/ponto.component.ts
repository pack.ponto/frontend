import { Component, Input, OnInit } from '@angular/core';
import { CollaboratorsDataService } from 'src/app/service/collaborators/collaborators-data.service';

@Component({
  selector: 'app-ponto',
  templateUrl: './ponto.component.html',
  styleUrls: ['./ponto.component.scss'],
})
export class PontoComponent implements OnInit {
  expedienteMesAtual: any;
  data: any;

  @Input() drawerAberto: boolean = false;

  constructor(private dado: CollaboratorsDataService) {}

  async ngOnInit(): Promise<void> {
    try {
      await this.dado.obterColaborador().toPromise();
    } catch (error) {
      await this.dado.cadastrarColaboradores().toPromise();
    }
  }
}
