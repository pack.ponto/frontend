import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  colaborador: any;
  dataExpediente: any;
  expedienteMensal: any;

  @Input() drawerAberto: boolean = true;

  constructor() {}

  ngOnInit(): void {
    this.checarTamanhoTela();
  }

  private checarTamanhoTela() {
    if (window.innerWidth <= 720) {
      this.drawerAberto = false;
    }
  }
}
