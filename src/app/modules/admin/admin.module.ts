import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SearchModule } from './search/search.module';
import { InfoModule } from './info/info.module';
import { NavbarModule } from '../navbar/navbar.module';



@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    NavbarModule,
    MatSidenavModule,
    SearchModule,
    InfoModule
  ],
  exports: [
    AdminComponent
  ]
})
export class AdminModule { }
