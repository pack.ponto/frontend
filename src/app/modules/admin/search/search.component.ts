import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
import { WorkdayDataService } from 'src/app/service/workday/workday-data.service';
import { CollaboratorsDataService } from 'src/app/service/collaborators/collaborators-data.service';

const moment = _rollupMoment || _moment;

export const FORMATACAO_DATA = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: FORMATACAO_DATA },
  ],
})
export class SearchComponent implements OnInit {
  @ViewChild('datepicker')
  ElementoDatepicker!: ElementRef;

  @ViewChild('input')
  ElementoInput!: ElementRef;

  colaboradores: string[] = [];
  colaboradoresFiltrados: Observable<string[]> | undefined;

  colaboradorFormulario = new FormControl('', {
    validators: [this.validarAutocomplete(), Validators.required],
  });

  data = new FormControl(moment());

  expedienteMensal: any;

  @Output() colaboradorSelecionado: EventEmitter<any> = new EventEmitter();
  @Output() dataSelecionada: EventEmitter<any> = new EventEmitter();
  @Output() expedienteEncontrado: EventEmitter<any> = new EventEmitter();
  @Output() dadosEnviados: EventEmitter<any> = new EventEmitter();

  constructor(
    private dado: WorkdayDataService,
    private dadoColaborador: CollaboratorsDataService
  ) {}

  public mensagensValidacao = {
    ControleAutocompleteColaboradores: [
      {
        type: 'invalidAutocompleteString',
        message: 'Colaborador não cadastrado.',
      },
      { type: 'required', message: 'Selcione um colaborador.' },
    ],
  };

  async ngOnInit() {
    await this.obterColaboradores();
    this.colaboradoresFiltrados = this.colaboradorFormulario.valueChanges.pipe(
      startWith(''),
      map((valor) => this._filtrar(valor))
    );
  }

  private async obterColaboradores() {
    try {
      await this.dadoColaborador
        .obterListaColaboradores()
        .toPromise()
        .then((dado) => {
          this.colaboradores = JSON.parse(dado);
        });
    } catch (error) {
      console.log("Erro ao obter lista de colaboradores do banco de dados");
    }
  }

  private _filtrar(valor: string): string[] {
    const valorFiltro = valor.toLowerCase();

    return this.colaboradores.filter(
      (colaborador) => colaborador.toLowerCase().indexOf(valorFiltro) === 0
    );
  }

  private validarAutocomplete(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      let valorSelecionado = c.value;
      let escolhidoOunao = this.colaboradores.filter(
        (nome) => nome === valorSelecionado
      );
      if (escolhidoOunao.length <= 0 && c.value != '') {
        return { invalidAutocompleteString: true };
      }
      return null;
    };
  }

  public tratarAnoEscolhido(ano: Moment) {
    const valorData = this.data.value;
    valorData.year(ano.year());
    this.data.setValue(valorData);
  }

  public tratarMesEscolhido(mes: Moment, datepicker: MatDatepicker<Moment>) {
    const valorData = this.data.value;
    valorData.month(mes.month());
    this.data.setValue(valorData);
    datepicker.close();
    this.focarDatepicker();
  }

  private focarDatepicker() {
    setTimeout(() => {
      this.ElementoDatepicker.nativeElement.focus();
    }, 0);
  }

  public onSubmit() {
    this.desfocarDatepicker();
    this.requisitarColaborador();
    this.encontrarExpedienteMensal();
    this.checarTamanhoTela();
  }

  private desfocarDatepicker() {
    setTimeout(() => {
      this.ElementoDatepicker.nativeElement.blur();
    }, 0);
  }

  private requisitarColaborador() {
    if (this.colaboradorFormulario.value == '') {
      this.ElementoInput.nativeElement.focus();
      this.ElementoInput.nativeElement.blur();
    }
  }

  private async encontrarExpedienteMensal() {
    try {
      if (this.colaboradorFormulario.valid) {
        await this.dado
          .getExpedienteDoMes(
            this.colaboradorFormulario.value,
            this.data.value.month() + 1,
            this.data.value.year()
          )
          .toPromise()
          .then((dado) => {
            this.expedienteMensal = JSON.parse(dado);
          });
        this.passarValores();
      }
    } catch (error) {
      console.log("Erro ao obter expediente mensal do colaborador informado");
    }
  }

  private passarValores() {
    const meses: string[] = [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro',
    ];
    const mes: string = meses[this.data.value.month()];

    this.colaboradorSelecionado.emit(this.colaboradorFormulario.value);
    this.dataSelecionada.emit(mes + '/' + this.data.value.year());
    this.expedienteEncontrado.emit(this.expedienteMensal);
  }

  public checarTamanhoTela() {
    if (window.innerWidth <= 985 && this.colaboradorFormulario.valid) {
      this.dadosEnviados.emit(false);
    }
  }
}
