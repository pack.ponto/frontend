import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  painelEstado = false;

  colunas: string[] = ['tipoPonto', 'observacoes'];
  colunasFooter: string[] = ['status', 'tipoStatus'];

  @Input() colaborador: any;
  @Input() dataExpediente: any;
  @Input() expedienteMensal: any;

  constructor() {}

  ngOnInit(): void {}

  public verificarSeHoje = (data: string) => {
    const dia = new Date(data);

    const hoje = new Date();

    return (
      dia.getDate() == hoje.getDate() &&
      dia.getMonth() == hoje.getMonth() &&
      dia.getFullYear() == hoje.getFullYear()
    );
  };

  public formatarData(data: string) {
    var dateData = new Date(data);

    let dia = '' + dateData.getDate();
    let mes = '' + dateData.getMonth() + 1;
    const ano = dateData.getFullYear();

    if (mes.length < 2) {
      mes = '0' + mes;
    }
    if (dia.length < 2) {
      dia = '0' + dia;
    }

    return [dia, mes, ano].join('/');
  }
}
