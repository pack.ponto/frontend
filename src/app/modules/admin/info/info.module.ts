import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info.component';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';



@NgModule({
  declarations: [InfoComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule
  ],
  exports: [
    InfoComponent
  ]
})
export class InfoModule { }
