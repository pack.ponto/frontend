import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Output() botaoClicado: EventEmitter<any> = new EventEmitter();

  @Input() drawerAberto: boolean = false;

  colaborador: string = this.keycloakService.getUsername();

  permissao: string[] = this.keycloakService.getUserRoles();

  constructor(private keycloakService: KeycloakService) {}

  ngOnInit(): void {}

  onToggle() {
    if (this.drawerAberto) {
      this.botaoClicado.emit(false);
    } else {
      this.botaoClicado.emit(true);
    }
  }

  public logout(): void {
    this.keycloakService.logout();
  }
}
