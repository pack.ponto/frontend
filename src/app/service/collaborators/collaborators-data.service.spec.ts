import { TestBed } from '@angular/core/testing';

import { CollaboratorsDataService } from './collaborators-data.service';

describe('CollaboratorsDataService', () => {
  let service: CollaboratorsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CollaboratorsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
