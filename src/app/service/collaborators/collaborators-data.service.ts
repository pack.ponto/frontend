import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class CollaboratorsDataService {

  identificadorColaborador: string = this.keycloakService.getUsername();

  constructor(private http: HttpClient, private keycloakService: KeycloakService) { }

  obterListaColaboradores() {
    return this.http.post('http://ponto-projeto.herokuapp.com/colaboradores/obter-lista',
      {}, {responseType: "text"});
  }

  obterColaborador() {
    return this.http.post('http://ponto-projeto.herokuapp.com/colaboradores/obter',
      {"identificador": this.identificadorColaborador }, {responseType: "text"});
  }

  cadastrarColaboradores() {
    return this.http.post('http://ponto-projeto.herokuapp.com/colaboradores/cadastrar',
      {"identificador": this.identificadorColaborador }, {responseType: "text"});
  }
}
