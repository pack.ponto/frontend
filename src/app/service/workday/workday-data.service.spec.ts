import { TestBed } from '@angular/core/testing';

import { WorkdayDataService } from './workday-data.service';

describe('WorkdayDataService', () => {
  let service: WorkdayDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WorkdayDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
