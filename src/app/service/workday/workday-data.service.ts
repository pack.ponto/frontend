import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class WorkdayDataService {

  identificadorColaborador: string = this.keycloakService.getUsername();

  constructor(private http: HttpClient, private keycloakService: KeycloakService) { }  

  postHorarioDeEntrada(observacoes:string) {

    return this.http.post('http://ponto-projeto.herokuapp.com/expediente/registrar-entrada',
      {
        "identificadorColaborador": this.identificadorColaborador,
        "observacoes": observacoes
      }, {responseType: "text"});
      
  }

  patchHorarioDeAlmoco(observacoes:string) {

    return this.http.patch('http://ponto-projeto.herokuapp.com/expediente/registrar-almoco',
      {
        "identificadorColaborador": this.identificadorColaborador,
        "observacoes": observacoes
      }, {responseType: "text"});
      
  }

  patchHorarioDeRetorno(observacoes:string) {

    return this.http.patch('http://ponto-projeto.herokuapp.com/expediente/registrar-retorno',
      {
        "identificadorColaborador": this.identificadorColaborador,
        "observacoes": observacoes
      }, {responseType: "text"});
      
  }

  patchHorarioDeSaida(observacoes:string) {

    return this.http.patch('http://ponto-projeto.herokuapp.com/expediente/registrar-saida',
      {
        "identificadorColaborador": this.identificadorColaborador,
        "observacoes": observacoes
      }, {responseType: "text"});
      
  }

  getExpedienteDoDia() {

    return this.http.post('http://ponto-projeto.herokuapp.com/expediente/atual',
    {
      "identificador": this.identificadorColaborador
    }, {responseType: "text"});

  }

  getExpedienteDoMes(identificadorColaborador: string, mes: number, ano: number) {

    return this.http.post('http://ponto-projeto.herokuapp.com/expediente/mensal',
    {
      "identificadorColaborador": identificadorColaborador,
      "mes": mes,
      "ano": ano
    }, {responseType: "text"});

  }

}
